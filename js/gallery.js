var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var _Gallery_instances, _Gallery_carousel, _Gallery_close, _Gallery_figure, _Gallery_gallery, _Gallery_media, _Gallery_next, _Gallery_prev, _Gallery_state, _Gallery_trigger, _Gallery_onKeyDown, _Gallery_getMedia, _Gallery_render, _Gallery_onLikesInc, _Gallery_onLikesDec, _Gallery_onOpenCarousel, _Gallery_openCarousel, _Gallery_onCloseCarousel, _Gallery_closeCarousel, _Gallery_movePrev, _Gallery_moveNext, _Gallery_updateCarousel, _Gallery_imgResize, _Gallery_windowSize, _Gallery_keyboardInput, _Gallery_onPopState, _Gallery_onSort, _Gallery_sort, _Gallery_sortPopular, _Gallery_sortTitle, _Gallery_sortDate;
import { DropDown } from "./dropDown.js";
// Takes care of everything dynamic on the gallery and carousel that is on the photographer
// profile page.
//
// Saves history state so navigating with the back button becomes possible between images in the
// carousel as well as when changing the filters or the ordering.
//
export class Gallery {
    // @param media The list of media to display in this gallery.
    //
    constructor(media) {
        _Gallery_instances.add(this);
        _Gallery_carousel.set(this, void 0);
        _Gallery_close.set(this, void 0);
        _Gallery_figure.set(this, void 0);
        _Gallery_gallery.set(this, void 0);
        _Gallery_media.set(this, void 0);
        _Gallery_next.set(this, void 0);
        _Gallery_prev.set(this, void 0);
        _Gallery_state.set(this, void 0); // index in array, the current image in the carousel.
        _Gallery_trigger.set(this, void 0); // The thumbnail that triggered the carousel on click. Used to give it focus on carousel close.
        // Store the event listener for keyboard events. As we bind it, we need to store
        // it so we can remove the event listener again.
        //
        _Gallery_onKeyDown.set(this, void 0);
        __classPrivateFieldSet(this, _Gallery_prev, document.querySelector(".carousel .prev"), "f");
        __classPrivateFieldSet(this, _Gallery_next, document.querySelector(".carousel .next"), "f");
        __classPrivateFieldSet(this, _Gallery_close, document.querySelector(".carousel .close"), "f");
        __classPrivateFieldSet(this, _Gallery_gallery, document.querySelector(".gallery--content"), "f");
        __classPrivateFieldSet(this, _Gallery_carousel, document.querySelector(".carousel"), "f");
        __classPrivateFieldSet(this, _Gallery_figure, __classPrivateFieldGet(this, _Gallery_carousel, "f").querySelector("figure"), "f");
        __classPrivateFieldGet(this, _Gallery_prev, "f").addEventListener("click", __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_movePrev).bind(this));
        __classPrivateFieldGet(this, _Gallery_next, "f").addEventListener("click", __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_moveNext).bind(this));
        __classPrivateFieldGet(this, _Gallery_close, "f").addEventListener("click", __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_onCloseCarousel).bind(this));
        window.addEventListener("resize", __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_imgResize).bind(this));
        window.addEventListener("popstate", __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_onPopState).bind(this));
        __classPrivateFieldSet(this, _Gallery_onKeyDown, __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_keyboardInput).bind(this), "f");
        const dropContainer = document.querySelector("#sort");
        const entries = ["Popularité", "Date", "Titre"];
        new DropDown(dropContainer, "Trier par", entries, __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_onSort).bind(this));
        __classPrivateFieldSet(this, _Gallery_media, media, "f");
        const filtered = [...Array(media.length).keys()];
        __classPrivateFieldSet(this, _Gallery_state, new GalleryState(null, filtered, "Popularité"), "f");
        __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_sort).call(this);
        // We don't want to create a new state right after loading the page, but we want to save this
        // because popsate will be invoked when the back button is pressed.
        //
        __classPrivateFieldGet(this, _Gallery_state, "f").replace();
        function sum(accu, m) {
            return accu + m.likes;
        }
        this.totalLikes = __classPrivateFieldGet(this, _Gallery_media, "f").reduce(sum, 0);
        document.body.addEventListener("open-carousel", { handleEvent: __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_onOpenCarousel).bind(this) });
        document.body.addEventListener("likes-increment", { handleEvent: __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_onLikesInc).bind(this) });
        document.body.addEventListener("likes-decrement", { handleEvent: __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_onLikesDec).bind(this) });
    }
}
_Gallery_carousel = new WeakMap(), _Gallery_close = new WeakMap(), _Gallery_figure = new WeakMap(), _Gallery_gallery = new WeakMap(), _Gallery_media = new WeakMap(), _Gallery_next = new WeakMap(), _Gallery_prev = new WeakMap(), _Gallery_state = new WeakMap(), _Gallery_trigger = new WeakMap(), _Gallery_onKeyDown = new WeakMap(), _Gallery_instances = new WeakSet(), _Gallery_getMedia = function _Gallery_getMedia(mIdx) {
    console.assert(mIdx <= __classPrivateFieldGet(this, _Gallery_media, "f").length, mIdx, __classPrivateFieldGet(this, _Gallery_media, "f").length);
    return __classPrivateFieldGet(this, _Gallery_media, "f")[mIdx];
}, _Gallery_render = function _Gallery_render() {
    // Be idempotent.
    //
    __classPrivateFieldGet(this, _Gallery_gallery, "f").innerHTML = "";
    for (const mIdx of __classPrivateFieldGet(this, _Gallery_state, "f").filtered) {
        const m = __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_getMedia).call(this, mIdx);
        const thumb = m.thumbnail();
        __classPrivateFieldGet(this, _Gallery_gallery, "f").append(thumb);
    }
}, _Gallery_onLikesInc = function _Gallery_onLikesInc() {
    const totalLikes = document.querySelector("#likes-price > .likes");
    totalLikes.innerText = (++this.totalLikes).toString();
}, _Gallery_onLikesDec = function _Gallery_onLikesDec() {
    const totalLikes = document.querySelector("#likes-price > .likes");
    totalLikes.innerText = (--this.totalLikes).toString();
}, _Gallery_onOpenCarousel = function _Gallery_onOpenCarousel(evt) {
    const mIdx = __classPrivateFieldGet(this, _Gallery_media, "f").indexOf(evt.detail);
    const fIdx = __classPrivateFieldGet(this, _Gallery_state, "f").filtered.indexOf(mIdx);
    if (fIdx === -1)
        throw "Gallery: Trying to open carousel on an image which is not currently in the gallery.";
    __classPrivateFieldGet(this, _Gallery_state, "f").carousel = fIdx;
    __classPrivateFieldSet(this, _Gallery_trigger, evt.detail.thumbnail().querySelector("picture"), "f");
    __classPrivateFieldGet(this, _Gallery_state, "f").push();
    __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_openCarousel).call(this);
    __classPrivateFieldGet(this, _Gallery_next, "f").focus();
}, _Gallery_openCarousel = function _Gallery_openCarousel() {
    document.body.classList.add("body__carousel");
    document.addEventListener("keydown", __classPrivateFieldGet(this, _Gallery_onKeyDown, "f"));
    __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_updateCarousel).call(this);
}, _Gallery_onCloseCarousel = function _Gallery_onCloseCarousel() {
    __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_closeCarousel).call(this);
    __classPrivateFieldGet(this, _Gallery_state, "f").carousel = null;
    __classPrivateFieldGet(this, _Gallery_state, "f").push();
}, _Gallery_closeCarousel = function _Gallery_closeCarousel() {
    document.removeEventListener("keydown", __classPrivateFieldGet(this, _Gallery_onKeyDown, "f"));
    document.body.classList.remove("body__carousel");
    __classPrivateFieldGet(this, _Gallery_trigger, "f").focus();
}, _Gallery_movePrev = function _Gallery_movePrev() {
    // make sure the state is valid (open carousel).
    //
    if (__classPrivateFieldGet(this, _Gallery_state, "f").carousel === null)
        return;
    if (__classPrivateFieldGet(this, _Gallery_state, "f").filtered.length < 1)
        throw "Gallery.#movePrev: Empty gallery?";
    __classPrivateFieldGet(this, _Gallery_state, "f").prev();
    __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_updateCarousel).call(this);
    __classPrivateFieldGet(this, _Gallery_carousel, "f").setAttribute("role", "alert");
}, _Gallery_moveNext = function _Gallery_moveNext() {
    // make sure the state is valid (open carousel).
    //
    if (__classPrivateFieldGet(this, _Gallery_state, "f").carousel === null)
        return;
    __classPrivateFieldGet(this, _Gallery_state, "f").next();
    __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_updateCarousel).call(this);
    __classPrivateFieldGet(this, _Gallery_carousel, "f").setAttribute("role", "alert");
}, _Gallery_updateCarousel = function _Gallery_updateCarousel() {
    if (__classPrivateFieldGet(this, _Gallery_state, "f").carousel < 0)
        throw "negative index in gallery: " + __classPrivateFieldGet(this, _Gallery_state, "f").carousel;
    if (__classPrivateFieldGet(this, _Gallery_state, "f").carousel >= __classPrivateFieldGet(this, _Gallery_state, "f").filtered.length)
        throw "index in gallery bigger than length: " + __classPrivateFieldGet(this, _Gallery_state, "f").carousel;
    const mIdx = __classPrivateFieldGet(this, _Gallery_state, "f").filtered[__classPrivateFieldGet(this, _Gallery_state, "f").carousel];
    const m = __classPrivateFieldGet(this, _Gallery_media, "f")[mIdx];
    const content = __classPrivateFieldGet(this, _Gallery_figure, "f").querySelector(".media-content");
    const caption = __classPrivateFieldGet(this, _Gallery_figure, "f").querySelector("figcaption");
    content && content.remove();
    __classPrivateFieldGet(this, _Gallery_figure, "f").prepend(m.carousel());
    caption.innerText = m.title;
    __classPrivateFieldGet(this, _Gallery_carousel, "f").setAttribute("aria-label", `Image (${m.title}), closeup view`);
    __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_imgResize).call(this);
}, _Gallery_imgResize = function _Gallery_imgResize() {
    // carousel is not open.
    //
    if (__classPrivateFieldGet(this, _Gallery_state, "f").carousel === null)
        return;
    const mIdx = __classPrivateFieldGet(this, _Gallery_state, "f").filtered[__classPrivateFieldGet(this, _Gallery_state, "f").carousel];
    const m = __classPrivateFieldGet(this, _Gallery_media, "f")[mIdx];
    // Calculate the aspect ratio of the carousel:
    //
    const buttonWidth = __classPrivateFieldGet(this, _Gallery_prev, "f").offsetWidth * 2;
    const captionHeight = __classPrivateFieldGet(this, _Gallery_figure, "f").querySelector("figcaption").offsetHeight;
    const mAspect = m.naturalWidth() / m.naturalHeight();
    const viewport = __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_windowSize).call(this, buttonWidth, captionHeight);
    const hMargin = viewport.h / 20; // 5% of height
    const wMargin = viewport.w / 20; // 5% of width
    let width;
    // Image is wider than viewport
    //
    if (mAspect > viewport.aspect) {
        width = viewport.w - buttonWidth - wMargin;
    }
    // image is higher than viewport
    // We only substract a margin here, as the buttons on the side are properly spaced already.
    // However if we use the full height here, our image will touch the top of the screen and our
    // caption the bottom.
    //
    else {
        const height = viewport.h - captionHeight - hMargin;
        width = height * mAspect;
    }
    __classPrivateFieldGet(this, _Gallery_carousel, "f").style.setProperty("--carousel-width", Math.floor(width) + "px");
}, _Gallery_windowSize = function _Gallery_windowSize(buttonWidth, captionHeight) {
    const h = document.documentElement.clientHeight;
    const w = document.documentElement.clientWidth;
    // Take into account our button and caption sizes.
    //
    const aspect = (w - buttonWidth) / (h - captionHeight);
    return { h, w, aspect };
}, _Gallery_keyboardInput = function _Gallery_keyboardInput(evt) {
    switch (evt.key) {
        case "ArrowLeft":
            __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_movePrev).call(this);
            break;
        case "ArrowRight":
            __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_moveNext).call(this);
            break;
        case "Escape":
            __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_closeCarousel).call(this);
            break;
    }
}, _Gallery_onPopState = function _Gallery_onPopState(evt) {
    if (evt.state === null || typeof evt.state.gallery === "undefined")
        return;
    const state = evt.state.gallery;
    // The browser serializes the state object and does not respect class on deserialization. We need to
    // construct it again.
    //
    __classPrivateFieldSet(this, _Gallery_state, new GalleryState(state.carousel, state.filtered, state.order), "f");
    // Has to be before we render the carousel, as width and height on the image will
    // return 0 if it's not in the dom.
    //
    __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_render).call(this);
    __classPrivateFieldGet(this, _Gallery_state, "f").carousel === null ?
        __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_closeCarousel).call(this)
        : __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_openCarousel).call(this);
}, _Gallery_onSort = function _Gallery_onSort(val) {
    if (val === __classPrivateFieldGet(this, _Gallery_state, "f").order)
        return;
    __classPrivateFieldGet(this, _Gallery_state, "f").order = val;
    __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_sort).call(this);
    // The dropdown will do the push first.
    //
    __classPrivateFieldGet(this, _Gallery_state, "f").replace();
}, _Gallery_sort = function _Gallery_sort() {
    switch (__classPrivateFieldGet(this, _Gallery_state, "f").order) {
        case "Popularité":
            __classPrivateFieldGet(this, _Gallery_state, "f").filtered.sort(__classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_sortPopular).bind(this));
            break;
        case "Titre":
            __classPrivateFieldGet(this, _Gallery_state, "f").filtered.sort(__classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_sortTitle).bind(this));
            break;
        case "Date":
            __classPrivateFieldGet(this, _Gallery_state, "f").filtered.sort(__classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_sortDate).bind(this));
            break;
        default: throw "Unknown sort type.";
    }
    __classPrivateFieldGet(this, _Gallery_instances, "m", _Gallery_render).call(this);
}, _Gallery_sortPopular = function _Gallery_sortPopular(mIdx1, mIdx2) {
    const likes1 = __classPrivateFieldGet(this, _Gallery_media, "f")[mIdx1].likes;
    const likes2 = __classPrivateFieldGet(this, _Gallery_media, "f")[mIdx2].likes;
    return likes2 - likes1;
}, _Gallery_sortTitle = function _Gallery_sortTitle(mIdx1, mIdx2) {
    const title1 = __classPrivateFieldGet(this, _Gallery_media, "f")[mIdx1].title;
    const title2 = __classPrivateFieldGet(this, _Gallery_media, "f")[mIdx2].title;
    if (title2 < title1)
        return 1;
    if (title2 > title1)
        return -1;
    // They are identical.
    //
    return 0;
}, _Gallery_sortDate = function _Gallery_sortDate(mIdx1, mIdx2) {
    const date1 = __classPrivateFieldGet(this, _Gallery_media, "f")[mIdx1].date;
    const date2 = __classPrivateFieldGet(this, _Gallery_media, "f")[mIdx2].date;
    if (date2 < date1)
        return 1;
    if (date2 > date1)
        return -1;
    // They are identical.
    //
    return 0;
};
// Keeps track of whether the carousel is open, which image it's on and how the gallery is sorted.
// Modification of this state happens through setters, which will take care of calling window.history.pushState.
// This allows the back button to work when browsing through the carousel.
//
class GalleryState {
    constructor(carousel, filtered, order) {
        this.carousel = carousel;
        this.filtered = filtered;
        this.order = order;
    }
    // Decrement the index, looping round to the end. The caller has to guarantee that
    // carousel is not null.
    // @param max the highest valid index in the media array.
    // @return the new value.
    //
    prev() {
        if (this.carousel === null)
            throw "GalleryState: trying to substract from null carousel idx.";
        let previous = this.carousel - 1;
        if (previous < 0)
            previous = this.filtered.length - 1;
        this.carousel = previous;
        this.push();
        return this.carousel;
    }
    // Increment the index, looping round to the beginning. The caller has to guarantee that
    // carousel is not null.
    // @param length the length of the media array.
    // @return the new value.
    //
    next() {
        if (this.carousel === null)
            throw "GalleryState: trying to add to null carousel idx.";
        // Just loop round to the start.
        // Note that we do increment on _carousel to avoid triggering pushstate, as increment is an assignment.
        //
        this.carousel = ++this.carousel % this.filtered.length;
        this.push();
        return this.carousel;
    }
    push() {
        const state = window.history.state || {};
        state.gallery = this;
        window.history.pushState(state, document.title, "");
        return this;
    }
    replace() {
        const state = window.history.state || {};
        state.gallery = this;
        window.history.replaceState(state, document.title, "");
        return this;
    }
}
