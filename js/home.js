var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var _Home_instances, _Home_contenuLink, _Home_data, _Home_tagCloud, _Home_router, _Home_generateSummaries, _Home_onFilter, _Home_onScroll, _Home_filterQuery;
import { TagCloud } from "./tagCloud.js";
export class Home {
    constructor(data, router) {
        _Home_instances.add(this);
        _Home_contenuLink.set(this, void 0);
        _Home_data.set(this, void 0);
        _Home_tagCloud.set(this, void 0); // the one at the top.
        _Home_router.set(this, void 0);
        __classPrivateFieldSet(this, _Home_data, data, "f");
        __classPrivateFieldSet(this, _Home_router, router, "f");
        const nav = document.querySelector("header > nav");
        __classPrivateFieldSet(this, _Home_tagCloud, new TagCloud(Array.from(data.allTags())), "f");
        nav.append(__classPrivateFieldGet(this, _Home_tagCloud, "f").html("Filtrer les photographes par le mot clef: "));
        // https://github.com/microsoft/TypeScript/issues/28357#issuecomment-711131035
        //
        window.addEventListener("filter-change", { handleEvent: __classPrivateFieldGet(this, _Home_instances, "m", _Home_onFilter).bind(this) });
        __classPrivateFieldGet(this, _Home_instances, "m", _Home_generateSummaries).call(this);
        const filter = __classPrivateFieldGet(this, _Home_instances, "m", _Home_filterQuery).call(this);
        if (filter)
            __classPrivateFieldGet(this, _Home_tagCloud, "f").toggle(filter);
        __classPrivateFieldSet(this, _Home_contenuLink, document.getElementById("contenu-link"), "f");
        window.addEventListener("scroll", __classPrivateFieldGet(this, _Home_instances, "m", _Home_onScroll).bind(this));
    }
}
_Home_contenuLink = new WeakMap(), _Home_data = new WeakMap(), _Home_tagCloud = new WeakMap(), _Home_router = new WeakMap(), _Home_instances = new WeakSet(), _Home_generateSummaries = function _Home_generateSummaries() {
    const prof = document.getElementById("profiles");
    for (const p of __classPrivateFieldGet(this, _Home_data, "f").photographers) {
        prof.append(p.profileSummary());
    }
}, _Home_onFilter = function _Home_onFilter(evt) {
    const filters = evt.detail;
    let showAll = false;
    // if every filter is unset, consider them all set.
    //
    if ([...filters.values()].every(elem => !elem))
        showAll = true;
    const prof = document.getElementById("profiles");
    outer: for (const p of __classPrivateFieldGet(this, _Home_data, "f").photographers) {
        const div = prof.querySelector(`.profile-${p.id}`);
        if (!showAll) {
            for (const [tag, filterActive] of filters) {
                if (filterActive && !p.tags.includes(tag)) {
                    div.classList.add("hide");
                    continue outer;
                }
            }
        }
        div.classList.remove("hide");
    }
}, _Home_onScroll = function _Home_onScroll() {
    document.documentElement.scrollTop > 50 /*pixels*/ ?
        __classPrivateFieldGet(this, _Home_contenuLink, "f").classList.remove("hide")
        : __classPrivateFieldGet(this, _Home_contenuLink, "f").classList.add("hide");
}, _Home_filterQuery = function _Home_filterQuery() {
    const filter = __classPrivateFieldGet(this, _Home_router, "f").getQueryVar("filter");
    if (filter === null || filter === "")
        return null;
    const tags = [...__classPrivateFieldGet(this, _Home_data, "f").allTags()];
    if (!tags.includes(filter))
        throw `Trying to filter on unknown filter: ${filter}`;
    return filter;
};
