import { Media } from "./media.js";
import { Photographer } from "./photographer.js";
import * as util from "./util.js";
export class FishEyeData {
    constructor(json) {
        console.log(json);
        this.photographers = [];
        this.media = [];
        for (let p of json.photographers) {
            this.photographers.push(new Photographer(p));
        }
        for (let m of json.media) {
            this.media.push(Media.createMedia(m));
        }
        Object.freeze(this);
    }
    allTags() {
        let all = [];
        for (let p of this.photographers) {
            all = all.concat(p.tags);
        }
        return new Set(all.sort());
    }
    findPhotographer(id) {
        return this.photographers.find(p => p.id == id);
    }
    allTagsDom() {
        let tags = "";
        for (let t of this.allTags()) {
            tags += `<span class="tag" aria-label="Tag">${t}</span>`;
        }
        let html = `<div class="tags">${tags}</div>`;
        return util.fragment(html);
    }
    mediaByAuthor(p) {
        return this.media.filter(t => t.photographerID == p.id);
    }
}
