# Openclassrooms front-end projet 6 - Fisheye

Creez un site accessible pour une plateforme de photographes : [Démo FishEye](https://tycho_dekie.gitlab.io/TychoDekie_6_06092021).

Le projet mets en avant les compétences suivantes:

- Organiser un logiciel Javascript selon le pardigme OOP.
- Accessibilité (navigation clavier/lecteur d'écran).

